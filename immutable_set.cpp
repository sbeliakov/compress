#include "immutable_set.h"

#include <algorithm>
#include <cassert>
#include <iostream>

#include "constants.h"


ImmutableSet::ImmutableSet(std::vector<int> _numbers) {
    size = static_cast<int> (_numbers.size());
    numbers = new int[size];
    for (int i = 0; i < size; ++i) {
        numbers[i] = _numbers[i];
    }
}

ImmutableSet::ImmutableSet(ImmutableSet::ImmutableSetBuilder &builder) {
    builder.build(this);
}

ImmutableSet::ImmutableSet(int *_numbers, int _size)
    : numbers(_numbers)
    , size(_size) {
}

ImmutableSet::ImmutableSet(const ImmutableSet &other) {
    *this = other;
}

ImmutableSet &ImmutableSet::operator=(const ImmutableSet &other) {
    size = other.size;
    numbers = new int[size];
    for (int i = 0; i < size; ++i) {
        numbers[i] = other.numbers[i];
    }
    return *this;
}

ImmutableSet::~ImmutableSet() {
    delete[] numbers;
}

bool ImmutableSet::contains(int value) const {
    if (size == 0) return false;
    int leftPossible = 0;
    int rightImpossible = size;
    while (rightImpossible > leftPossible + 1) {
        int m = (leftPossible + rightImpossible) / 2;
        if (numbers[m] <= value) {
            leftPossible = m;
        }
        else {
            rightImpossible = m;
        }
    }
    return numbers[leftPossible] == value;
}

int ImmutableSet::testDataSize() const {
    return size * INT_BITS;
}


void ImmutableSet::ImmutableSetBuilder::add(int number) {
    buffer.push_back(number);
}

std::vector<int> ImmutableSet::ImmutableSetBuilder::prebuild() {
    std::sort(buffer.begin(), buffer.end());
    auto bufferEnd = std::unique(buffer.begin(), buffer.end());
    return std::vector<int>(buffer.begin(), bufferEnd);
}

ImmutableSet ImmutableSet::ImmutableSetBuilder::build() {
    auto result = prebuild();
    return ImmutableSet(result);
}

void ImmutableSet::ImmutableSetBuilder::build(ImmutableSet* set) {
    auto result = prebuild();
    new (set) ImmutableSet(result);
}
