#ifndef COMPRESS_IMMUTABLE_CREATIVE_BUCKET_H
#define COMPRESS_IMMUTABLE_CREATIVE_BUCKET_H

#include <vector>

#include "immutable_interval_set.h"
#include "immutable_set.h"
#include "immutable_cluster_set.h"
#include "serializer_prototype.h"


class ImmutableCreativeBucket {

    friend class DataSizeTester<ImmutableCreativeBucket>;
    friend class BinarySerializer<ImmutableCreativeBucket>;

  private:

    ImmutableCreativeBucket(
            ImmutableClusterSet::ImmutableClusterSetBuilder& clustersBuilder,
            ImmutableIntervalSet::ImmutableIntervalSetBuilder& intervalsBuilder,
            ImmutableSet::ImmutableSetBuilder& singlesBuilder);

    ImmutableCreativeBucket(
            const ImmutableClusterSet& _clusters,
            const ImmutableIntervalSet& _intervals,
            const ImmutableSet& _singles);

    int testDataSize() const;

  private:

    ImmutableClusterSet clusters;
    ImmutableIntervalSet intervals;
    ImmutableSet singles;

  public:

    bool contains(int value) const;

    ImmutableCreativeBucket(const ImmutableCreativeBucket& other);

    ImmutableCreativeBucket& operator= (const ImmutableCreativeBucket& other);

    ~ImmutableCreativeBucket();

  public:

    class ImmutableCreativeBucketBuilder;

};


class ImmutableCreativeBucket::ImmutableCreativeBucketBuilder {

  private:

    std::vector<int> buffer;
    ImmutableSet::ImmutableSetBuilder singlesBuilder;
    ImmutableIntervalSet::ImmutableIntervalSetBuilder intervalsBuilder;
    ImmutableClusterSet::ImmutableClusterSetBuilder clustersBuilder;

    static const int SINGLE = 0;
    static const int INTERVAL = 1;
    static const int CLUSTER = 2;

  public:

    void add(int number);

    ImmutableCreativeBucket build();

    void build(ImmutableCreativeBucket* bucket);

    ImmutableCreativeBucketBuilder();

    ~ImmutableCreativeBucketBuilder();

  private:

    void prebuild();

    std::vector<std::vector<int> > computeOptimalWeights();

};


#endif //COMPRESS_IMMUTABLE_CREATIVE_BUCKET_H
