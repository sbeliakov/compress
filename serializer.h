#ifndef COMPRESS_SERIALIZER_H
#define COMPRESS_SERIALIZER_H

#include "serializer_prototype.h"

#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <string>

#include "creative_storage.h"


template <class K, class V>
class BinarySerializer<std::unordered_map<K, V>> {

public:

    static void serialize(const std::unordered_map<K, V>& map, std::ostream& out);

    static std::unordered_map<K, V> deserialize(std::istream& in);

};


template <>
class BinarySerializer<std::string> {

public:

    static void serialize(const std::string& value, std::ostream& out);

    static std::string deserialize(std::istream& in);

};


template <>
class BinarySerializer<CreativeStorage> {

public:

    static void serialize(const CreativeStorage& value, std::ostream& out);

    static CreativeStorage deserialize(std::istream& in);

};


template <>
class BinarySerializer<CreativeBucketWithMeta> {

public:

    static void serialize(const CreativeBucketWithMeta& value, std::ostream& out);

    static CreativeBucketWithMeta deserialize(std::istream& in);

};


template <>
class BinarySerializer<ImmutableCreativeBucket> {

public:

    static void serialize(const ImmutableCreativeBucket& value, std::ostream& out);

    static ImmutableCreativeBucket deserialize(std::istream& in);

};


template <>
class BinarySerializer<ImmutableClusterSet> {

public:

    static void serialize(const ImmutableClusterSet& value, std::ostream& out);

    static ImmutableClusterSet deserialize(std::istream& in);

};


template <>
class BinarySerializer<ImmutableIntervalSet> {

public:

    static void serialize(const ImmutableIntervalSet& value, std::ostream& out);

    static ImmutableIntervalSet deserialize(std::istream& in);

};


template <>
class BinarySerializer<ImmutableSet> {

public:

    static void serialize(const ImmutableSet& value, std::ostream& out);

    static ImmutableSet deserialize(std::istream& in);

};


template <class K, class V>
void BinarySerializer<std::unordered_map<K, V>>::serialize(const std::unordered_map<K, V>& map, std::ostream& out) {
    BinarySerializer<size_t>::serialize(map.size(), out);
    for (auto kv: map) {
        BinarySerializer<K>::serialize(kv.first, out);
        BinarySerializer<V>::serialize(kv.second, out);
    }
}

template <class K, class V>
std::unordered_map<K, V> BinarySerializer<std::unordered_map<K, V>>::deserialize(std::istream& in) {
    size_t mapSize = BinarySerializer<size_t>::deserialize(in);
    std::unordered_map<K, V> result;
    for (int i = 0; i < mapSize; ++i) {
        K k = BinarySerializer<K>::deserialize(in);
        V v = BinarySerializer<V>::deserialize(in);
        result.insert(std::make_pair(k, v));
    }
    return result;
}

#endif //COMPRESS_SERIALIZER_H