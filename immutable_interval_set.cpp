#include "immutable_interval_set.h"

#include <algorithm>
#include <iostream>

#include "test/test.h"
#include "constants.h"


ImmutableIntervalSet::ImmutableIntervalSet(std::vector<int>& _endings) {
    size = static_cast<int> (_endings.size());
    endings = new int[size];
    for (int i = 0; i < size; ++i) {
        endings[i] = _endings[i];
    }
}

ImmutableIntervalSet::ImmutableIntervalSet(ImmutableIntervalSet::ImmutableIntervalSetBuilder &builder) {
    builder.build(this);
}

ImmutableIntervalSet::ImmutableIntervalSet(const ImmutableIntervalSet &other) {
    *this = other;
}

ImmutableIntervalSet::ImmutableIntervalSet(int _size, int *_endings)
    : size(_size)
    , endings(_endings) {
}

ImmutableIntervalSet &ImmutableIntervalSet::operator=(const ImmutableIntervalSet &other) {
    size = other.size;
    endings = new int[size];
    for (int i = 0; i < size; ++i) {
        endings[i] = other.endings[i];
    }
    return *this;
}

ImmutableIntervalSet::~ImmutableIntervalSet() {
    delete[] endings;
}

bool ImmutableIntervalSet::contains(int value) const {
    if (size == 0) return false;
    if (value < endings[0]) return false;
    int leftLe = 0;
    int rightGt = size;
    while (rightGt > leftLe + 1) {
        int m = (leftLe + rightGt) / 2;
        if (endings[m] <= value) {
            leftLe = m;
        }
        else {
            rightGt = m;
        }
    }
    // should be the left end of segment
    return leftLe % 2 == 0;
}

int ImmutableIntervalSet::testDataSize() const {
    return size * INT_BITS;
}


ImmutableIntervalSet::ImmutableIntervalSetBuilder::Ending::Ending(int _number, bool _isBegin)
    : number(_number)
    , isBegin(_isBegin) {
}

bool ImmutableIntervalSet::ImmutableIntervalSetBuilder::Ending::operator < (const Ending& other) const {
    if (number != other.number) return number < other.number;
    if (isBegin != other.isBegin) return isBegin;
    // here they must be equal
    return false;
}


void ImmutableIntervalSet::ImmutableIntervalSetBuilder::add(int begin, int end) {
    if (begin < end) {
        endings.push_back(Ending(begin, true));
        endings.push_back(Ending(end, false));
    }
}

std::vector<int> ImmutableIntervalSet::ImmutableIntervalSetBuilder::prebuild() {
    std::sort(endings.begin(), endings.end());
    std::vector<int> result;
    for (auto it = endings.begin(); it != endings.end(); ++it) {
        result.push_back(it->number);
        int balance = 1;
        while (balance > 0) {
            ++it;
            if (it->isBegin) balance += 1;
            else balance -= 1;
        }
        result.push_back(it->number);
    }
    return result;
}

ImmutableIntervalSet ImmutableIntervalSet::ImmutableIntervalSetBuilder::build() {
    auto result = prebuild();
    return ImmutableIntervalSet(result);
}

void ImmutableIntervalSet::ImmutableIntervalSetBuilder::build(ImmutableIntervalSet* set) {
    auto result = prebuild();
    new (set) ImmutableIntervalSet(result);
}
