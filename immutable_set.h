#ifndef COMPRESS_IMMUTABLE_SET_H
#define COMPRESS_IMMUTABLE_SET_H

#include <vector>

#include "test/test.h"
#include "serializer_prototype.h"


class ImmutableSet {

    friend class DataSizeTester<ImmutableSet>;
    friend class BinarySerializer<ImmutableSet>;

  public:

    class ImmutableSetBuilder;

  private:

    int* numbers;
    int size;

  private:

    ImmutableSet(std::vector<int> _numbers);

    ImmutableSet(int* _numbers, int _size);

    int testDataSize() const;

  public:

    ImmutableSet(ImmutableSetBuilder& builder);

    ImmutableSet(const ImmutableSet& other);

    ImmutableSet& operator= (const ImmutableSet& other);

    ~ImmutableSet();

    bool contains(int value) const;

};


class ImmutableSet::ImmutableSetBuilder {

  private:

    std::vector<int> buffer;

  public:

    void add(int number);

    ImmutableSet build();

    void build(ImmutableSet* set);

  private:

    std::vector<int> prebuild();

};


#endif //COMPRESS_IMMUTABLE_SET_H
