#ifndef COMPRESS_CREATIVE_STORAGE_H
#define COMPRESS_CREATIVE_STORAGE_H

#include <unordered_map>
#include <string>
#include <algorithm>
#include <iostream>

#include "serializer_prototype.h"
#include "immutable_creative_bucket.h"


typedef std::pair<int, int> CreativeBucketKey;

class CreativeStorage;
class CreativeBucketWithMeta;


class CreativeBucketWithMeta {

    friend class BinarySerializer<CreativeBucketWithMeta>;

private:

    ImmutableCreativeBucket bucket;
    int meta;

    static const int IS_EXCLUDE = 1;
    static const int IS_INT = 2;

public:

    CreativeBucketWithMeta(const ImmutableCreativeBucket& _bucket, int _meta);

    CreativeBucketWithMeta& operator= (const CreativeBucketWithMeta& other);

    const ImmutableCreativeBucket& getBucket();

    bool isExclude();

    bool isInt();

    static bool isExclude(int mask);

    static bool isInt(int mask);

};


namespace std
{
    template<>
    struct hash<std::pair<int, int> >
    {
        typedef std::pair<int, int> argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& s) const
        {
            return static_cast<size_t> (s.first ^ (s.second << 1));
        }
    };
}


class CreativeStorage {
    
    friend class BinarySerializer<CreativeStorage>;

  private:

    std::unordered_map<CreativeBucketKey, CreativeBucketWithMeta> buckets;
    std::unordered_map<std::string, int> strCreativesToInt;
    int lastFreeAlias;
    
  private:
    
    CreativeStorage(
            const std::unordered_map<CreativeBucketKey, CreativeBucketWithMeta>& _buckets,
            const std::unordered_map<std::string, int>& _strCreativesToInt) 
        : buckets(_buckets)
        , strCreativesToInt(_strCreativesToInt) {
    }

  public:

    CreativeStorage();

    bool isApproved(int dspId, int siteId, std::string creativeId);

    int getIntValue(const std::string& strValue, bool isInt, bool createIfNotFound);

    bool isApproved(const CreativeBucketKey& key, std::string creativeId);

    void parseFromStream(std::istream& input);

};

#endif //COMPRESS_CREATIVE_STORAGE_H
