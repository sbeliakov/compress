#include "immutable_cluster_set.h"

#include <algorithm>
#include <iostream>

#include "constants.h"


ImmutableClusterSet::ImmutableClusterSet(const std::vector<int>& _clusterBegin, const std::vector<int>& _clusterBeginPos, const std::vector<int>& _mask, int _maskSize) {
    numClusters = _clusterBegin.size();
    clusterBegin = new int[numClusters];
    clusterBeginPos = new int[numClusters];
    mask = new int[_mask.size()];
    maskSize = _maskSize;
    for (int i = 0; i < numClusters; ++i) {
        clusterBegin[i] = _clusterBegin[i];
        clusterBeginPos[i] = _clusterBeginPos[i];
    }
    for (int i = 0; i < _mask.size(); ++i) {
        mask[i] = _mask[i];
    }
}

ImmutableClusterSet::ImmutableClusterSet(ImmutableClusterSet::ImmutableClusterSetBuilder& builder) {
    builder.build(this);
}

ImmutableClusterSet::ImmutableClusterSet(const ImmutableClusterSet &other) {
    *this = other;
}

ImmutableClusterSet::ImmutableClusterSet(int *_clusterBegin, int *_clusterBeginPos, int *_mask, int _numClusters, int _maskSize)
    : clusterBegin(_clusterBegin)
    , clusterBeginPos(_clusterBeginPos)
    , mask(_mask)
    , numClusters(_numClusters)
    , maskSize(_maskSize) {
}

ImmutableClusterSet &ImmutableClusterSet::operator=(const ImmutableClusterSet &other) {
    numClusters = other.numClusters;
    clusterBegin = new int[numClusters];
    clusterBeginPos = new int[numClusters];
    maskSize = other.maskSize;
    int maskInts = (maskSize + INT_BITS - 1) / (INT_BITS);
    mask = new int[maskInts];
    for (int i = 0; i < numClusters; ++i) {
        clusterBegin[i] = other.clusterBegin[i];
        clusterBeginPos[i] = other.clusterBeginPos[i];
    }
    for (int i = 0; i < maskInts; ++i) {
        mask[i] = other.mask[i];
    }
    return *this;
}

bool ImmutableClusterSet::contains(int value) const {
    // ToDo: move out binary search to separate piece of code
    if (numClusters == 0) return false;
    if (value < clusterBegin[0]) return false;
    int leftLe = 0;
    int rightGt = numClusters;
    while (leftLe + 1 < rightGt) {
        int m = (leftLe + rightGt) / 2;
        if (clusterBegin[m] <= value) {
            leftLe = m;
        }
        else {
            rightGt = m;
        }
    }
    int diff = value - clusterBegin[leftLe];
    // if value is the beginning of cluster
    if (diff == 0) return true;
    int shift = diff - 1;
    int bitPos = clusterBeginPos[leftLe] + shift;
    // if bit position is out of bit mask
    if (bitPos >= maskSize) return false;
    // if bit belongs to another cluster
    if (rightGt < numClusters && bitPos >= clusterBeginPos[rightGt]) return false;
    int numPos = bitPos / INT_BITS;
    int bitShift = bitPos % INT_BITS;
    return (mask[numPos] >> bitShift) & 1;
}

ImmutableClusterSet::~ImmutableClusterSet() {
    delete[] clusterBegin;
    delete[] clusterBeginPos;
    delete[] mask;
}

int ImmutableClusterSet::testDataSize() const {
    return numClusters * 2 * INT_BITS + maskSize;
}


ImmutableClusterSet::ImmutableClusterSetBuilder::ImmutableClusterSetBuilder()
    : maskSize(0) {
}

void ImmutableClusterSet::ImmutableClusterSetBuilder::addClusterByDiffs(int begin, const std::vector<int>& diffs) {
    buffer.push_back(Cluster(begin, diffs));
}

void ImmutableClusterSet::ImmutableClusterSetBuilder::addNextClusterByDiffs(int begin, const std::vector<int>& diffs) {
    clusterBegin.push_back(begin);
    clusterBeginPos.push_back(maskSize);
    for (const auto& diff : diffs) {
        addToBitMask(mask, maskSize + diff - 1);
        maskSize += diff;
    }
}

void ImmutableClusterSet::ImmutableClusterSetBuilder::addToBitMask(std::vector<int>& dest, int pos) {
    int numPos = pos / INT_BITS;
    int bitPos = pos % INT_BITS;
    while (dest.size() <= numPos) dest.push_back(0);
    dest.back() |= 1 << bitPos;
}

void ImmutableClusterSet::ImmutableClusterSetBuilder::prebuild() {
    std::sort(buffer.begin(), buffer.end());
    for (const auto& cluster: buffer) {
        addNextClusterByDiffs(cluster.begin, cluster.diffs);
    }
}

ImmutableClusterSet ImmutableClusterSet::ImmutableClusterSetBuilder::build() {
    prebuild();
    return ImmutableClusterSet(clusterBegin, clusterBeginPos, mask, maskSize);
}

void ImmutableClusterSet::ImmutableClusterSetBuilder::build(ImmutableClusterSet* clusterSet) {
    prebuild();
    new (clusterSet) ImmutableClusterSet(clusterBegin, clusterBeginPos, mask, maskSize);
}


ImmutableClusterSet::ImmutableClusterSetBuilder::Cluster::Cluster(int _begin, const std::vector<int>& _diffs)
        : begin(_begin)
        , diffs(_diffs) {
}

bool ImmutableClusterSet::ImmutableClusterSetBuilder::Cluster::operator < (const Cluster& other) const {
    return begin < other.begin;
}
