#ifndef COMPRESS_SERIALIZER_PROTOTYPE_H
#define COMPRESS_SERIALIZER_PROTOTYPE_H

#include <iostream>


// should work fine for simple data types
template <class T>
class BinarySerializer {

public:

    static void serialize(const T& value, std::ostream& out);

    static T deserialize(std::istream& in);

};


template<class T>
void BinarySerializer<T>::serialize(const T& value, std::ostream& out) {
    out.write(reinterpret_cast<const char*> (&value), sizeof(T));
}

template<class T>
T BinarySerializer<T>::deserialize(std::istream& in) {
    T result;
    in.read(reinterpret_cast<char*> (&result), sizeof(T));
    return result;
}

#endif //COMPRESS_SERIALIZER_PROTOTYPE_H
