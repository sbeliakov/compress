#include "creative_storage.h"

#include <unordered_map>
#include <string>
#include <algorithm>
#include <iostream>

#include "immutable_creative_bucket.h"


CreativeStorage::CreativeStorage()
  : lastFreeAlias(0) {
}

bool CreativeStorage::isApproved(int dspId, int siteId, std::string creativeId) {
    return isApproved(std::make_pair(dspId, siteId), creativeId);
}

int CreativeStorage::getIntValue(const std::string& strValue, bool isInt, bool createIfNotFound) {
    if (isInt) return atoi(strValue.c_str());
    if (strCreativesToInt.find(strValue) != strCreativesToInt.end()) return strCreativesToInt[strValue];
    if (!createIfNotFound) return -1;
    strCreativesToInt[strValue] = lastFreeAlias;
    return lastFreeAlias++;
}

bool CreativeStorage::isApproved(const CreativeBucketKey& key, std::string creativeId) {
    // ToDo: If needed may check in precounted multiset of all ints whether such num exists anywhere
    auto bucket = buckets.find(key);
    if (bucket == buckets.end()) return false;
    int value = getIntValue(creativeId, bucket->second.isInt(), false);
    return bucket->second.getBucket().contains(value);
}

void CreativeStorage::parseFromStream(std::istream& input) {
    int numBuckets;
    input >> numBuckets;
    for (int i = 0; i < numBuckets; ++i) {
        int dspId, siteId, meta;
        input >> dspId >> siteId >> meta;
        CreativeBucketKey key = std::make_pair(dspId, siteId);
        int numValues;
        input >> numValues;
        ImmutableCreativeBucket::ImmutableCreativeBucketBuilder builder;
        for (int j = 0; j < numValues; ++j) {
            std::string creative;
            input >> creative;
            int value = getIntValue(creative, CreativeBucketWithMeta::isInt(meta), true);
            builder.add(value);
        }
        buckets.insert(std::make_pair(key, CreativeBucketWithMeta(builder.build(), meta)));
    }
}


CreativeBucketWithMeta::CreativeBucketWithMeta(const ImmutableCreativeBucket& _bucket, int _meta)
        : bucket(_bucket)
        , meta(_meta) {
}

CreativeBucketWithMeta& CreativeBucketWithMeta::operator= (const CreativeBucketWithMeta& other) {
    bucket = other.bucket;
    meta = other.meta;
    return *this;
}

const ImmutableCreativeBucket& CreativeBucketWithMeta::getBucket() {
    return bucket;
}

bool CreativeBucketWithMeta::isExclude() {
    return isExclude(meta);
}

bool CreativeBucketWithMeta::isInt() {
    return isInt(meta);
}

bool CreativeBucketWithMeta::isExclude(int mask) {
    return static_cast<bool> (mask & IS_EXCLUDE);
}

bool CreativeBucketWithMeta::isInt(int mask) {
    return static_cast<bool> (mask & IS_INT);
}
