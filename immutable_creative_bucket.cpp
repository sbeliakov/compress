#include "immutable_creative_bucket.h"

#include <algorithm>
#include <vector>

#include "immutable_interval_set.h"
#include "immutable_set.h"
#include "immutable_cluster_set.h"
#include "constants.h"


ImmutableCreativeBucket::ImmutableCreativeBucket(
        ImmutableClusterSet::ImmutableClusterSetBuilder& clustersBuilder,
        ImmutableIntervalSet::ImmutableIntervalSetBuilder& intervalsBuilder,
        ImmutableSet::ImmutableSetBuilder& singlesBuilder)
    : clusters(clustersBuilder)
    , intervals(intervalsBuilder)
    , singles(singlesBuilder) {
}

ImmutableCreativeBucket::ImmutableCreativeBucket(const ImmutableCreativeBucket &other)
    : clusters(other.clusters)
    , intervals(other.intervals)
    , singles(other.singles) {
}

ImmutableCreativeBucket::ImmutableCreativeBucket(
        const ImmutableClusterSet &_clusters,
        const ImmutableIntervalSet &_intervals,
        const ImmutableSet &_singles)
        : clusters(_clusters)
        , intervals(_intervals)
        , singles(_singles) {
}

ImmutableCreativeBucket &ImmutableCreativeBucket::operator=(const ImmutableCreativeBucket &other) {
    clusters = other.clusters;
    intervals = other.intervals;
    singles = other.singles;
    return *this;
}

bool ImmutableCreativeBucket::contains(int value) const {
    return clusters.contains(value) ||
           intervals.contains(value) ||
           singles.contains(value);
}

ImmutableCreativeBucket::~ImmutableCreativeBucket() {
}

int ImmutableCreativeBucket::testDataSize() const {
    return DataSizeTester<ImmutableClusterSet>::testDataSize(clusters) +
            DataSizeTester<ImmutableIntervalSet>::testDataSize(intervals) +
            DataSizeTester<ImmutableSet>::testDataSize(singles);
}


ImmutableCreativeBucket::ImmutableCreativeBucketBuilder::ImmutableCreativeBucketBuilder() {
}

ImmutableCreativeBucket::ImmutableCreativeBucketBuilder::~ImmutableCreativeBucketBuilder() {
}

void ImmutableCreativeBucket::ImmutableCreativeBucketBuilder::add(int number) {
    buffer.push_back(number);
}

std::vector<std::vector<int> > ImmutableCreativeBucket::ImmutableCreativeBucketBuilder::computeOptimalWeights() {
    std::vector<std::vector<int> > minWeight(buffer.size(), std::vector<int>(3, 0));
    minWeight[0][SINGLE] = INT_BITS;
    minWeight[0][INTERVAL] = 2 * INT_BITS;
    minWeight[0][CLUSTER] = 2 * INT_BITS;
    for (int i = 1; i < minWeight.size(); ++i) {
        int bestPrevious = std::min(
                minWeight[i - 1][SINGLE], std::min(
                minWeight[i - 1][INTERVAL],
                minWeight[i - 1][CLUSTER]));
        // SINGLE section
        minWeight[i][SINGLE] = static_cast<int> (bestPrevious + INT_BITS);
        // INTERVAL section
        minWeight[i][INTERVAL] = static_cast<int> (bestPrevious + 2 * INT_BITS);
        if (buffer[i] - buffer[i - 1] == 1) {
            minWeight[i][INTERVAL] = std::min(minWeight[i][INTERVAL], minWeight[i - 1][INTERVAL]);
        }
        // CLUSTER section
        minWeight[i][CLUSTER] = std::min(
                static_cast<int> (bestPrevious + 2 * INT_BITS),
                minWeight[i - 1][CLUSTER] + buffer[i] - buffer[i - 1]);
    }
    return minWeight;
}

void ImmutableCreativeBucket::ImmutableCreativeBucketBuilder::prebuild() {
    if (buffer.empty()) {
        return;
    }
    std::sort(buffer.begin(), buffer.end());
    auto bufferEnd = std::unique(buffer.begin(), buffer.end());
    buffer.resize(static_cast<unsigned long> (bufferEnd - buffer.begin()));

    singlesBuilder = ImmutableSet::ImmutableSetBuilder();
    intervalsBuilder = ImmutableIntervalSet::ImmutableIntervalSetBuilder();
    clustersBuilder = ImmutableClusterSet::ImmutableClusterSetBuilder();

    auto minWeight = computeOptimalWeights();

    for (int i = static_cast<int> (minWeight.size() - 1); i >= 0; --i) {
        int minValue = std::min(
                minWeight[i][SINGLE], std::min(
                minWeight[i][INTERVAL],
                minWeight[i][CLUSTER]));

        if (minValue == minWeight[i][SINGLE]) {
            singlesBuilder.add(buffer[i]);
        }
        else if (minValue == minWeight[i][INTERVAL]) {
            int intervalEnd = buffer[i] + 1; // exclusive
            while (i > 0 && // has previous
                   buffer[i] - buffer[i - 1] == 1 && // may continue interval
                   minWeight[i - 1][INTERVAL] == minWeight[i][INTERVAL]) { // make sure it is the same interval
                --i;
            }
            intervalsBuilder.add(buffer[i], intervalEnd);
        }
        else { // minValue == minWeight[i][CLUSTER]
            auto clusterEnd = buffer.begin() + i + 1; // exclusive end
            while (i > 0 && // has previous
                   minWeight[i][CLUSTER] - minWeight[i - 1][CLUSTER] == buffer[i] - buffer[i - 1]) { // make sure it is the same cluster
                --i;
            }
            auto clusterBegin = buffer.begin() + i;
            std::vector<int> clusterBuffer(clusterBegin, clusterEnd);
            std::vector<int> clusterDiffs(clusterBuffer.size() - 1);
            for (int j = 0; j < clusterDiffs.size(); ++j) {
                clusterDiffs[j] = clusterBuffer[j + 1] - clusterBuffer[j];
            }
            clustersBuilder.addClusterByDiffs(clusterBuffer.front(), clusterDiffs);
        }
    }

}

ImmutableCreativeBucket ImmutableCreativeBucket::ImmutableCreativeBucketBuilder::build() {
    prebuild();
    return ImmutableCreativeBucket(clustersBuilder, intervalsBuilder, singlesBuilder);
}

void ImmutableCreativeBucket::ImmutableCreativeBucketBuilder::build(ImmutableCreativeBucket *bucket) {
    prebuild();
    new (bucket) ImmutableCreativeBucket(clustersBuilder, intervalsBuilder, singlesBuilder);
}
