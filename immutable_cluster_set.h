#ifndef COMPRESS_IMMUTABLE_CLUSTER_SET_H
#define COMPRESS_IMMUTABLE_CLUSTER_SET_H

#include <vector>

#include "test/test.h"
#include "serializer_prototype.h"

class ImmutableClusterSet {

    friend class DataSizeTester<ImmutableClusterSet>;
    friend class BinarySerializer<ImmutableClusterSet>;

  public:

    class ImmutableClusterSetBuilder;

  private:

    int* clusterBegin;
    int* clusterBeginPos;
    int* mask;
    int numClusters;
    int maskSize;

  private:

    ImmutableClusterSet(const std::vector<int>& _clusterBegin, const std::vector<int>& _clusterBeginPos, const std::vector<int>& _mask, int _maskSize);

    ImmutableClusterSet(int* _clusterBegin, int* _clusterBeginPos, int* _mask, int _numClusters, int _maskSize);

    int testDataSize() const;

  public:

    ImmutableClusterSet(ImmutableClusterSetBuilder& builder);

    ImmutableClusterSet(const ImmutableClusterSet& other);

    ImmutableClusterSet& operator= (const ImmutableClusterSet& other);

    bool contains(int value) const;

    ~ImmutableClusterSet();

};


class ImmutableClusterSet::ImmutableClusterSetBuilder {

  private:

    class Cluster;

  private:

    std::vector<Cluster> buffer;
    std::vector<int> clusterBegin;
    std::vector<int> clusterBeginPos;
    std::vector<int> mask;
    int maskSize;

  private:

    void addToBitMask(std::vector<int>& dest, int pos);

    void addNextClusterByDiffs(int begin, const std::vector<int>& diffs);

    void prebuild();

public:

    ImmutableClusterSetBuilder();

    void addClusterByDiffs(int begin, const std::vector<int>& diffs);

    ImmutableClusterSet build();

    void build(ImmutableClusterSet* clusterSet);
};


class ImmutableClusterSet::ImmutableClusterSetBuilder::Cluster {

  public:

    int begin;
    std::vector<int> diffs;

  public:

    Cluster(int _begin, const std::vector<int> &_diffs);

    bool operator<(const Cluster &other) const;

};

#endif //COMPRESS_IMMUTABLE_CLUSTER_SET_H
