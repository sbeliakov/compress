#include <iostream>
#include <fstream>

#include "creative_storage.h"
#include "serializer.h"
#include "test/run_tests.h"

using namespace std;


int main() {
    test();
    return 0;

    std::ifstream input("data/creatives-v2");
    CreativeStorage storage;
    storage.parseFromStream(input);
    std::ofstream output("data/creatives-v2.bin", std::ios::binary | std::ios::out);
    //std::ofstream output("data/creatives.bin", std::ios::out);
    std::cerr << "Starting dump\n";
    //std::ofstream* outputPtr = &output;
    //SpacedOfstream out(outputPtr);
    //storage.dumpToStreamCompressed(output);
    BinarySerializer<CreativeStorage>::serialize(storage, output);
    return 0;
}
