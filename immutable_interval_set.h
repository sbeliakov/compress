#ifndef COMPRESS_IMMUTABLE_INTERVAL_SET_H
#define COMPRESS_IMMUTABLE_INTERVAL_SET_H

#include <vector>

#include "test/test.h"
#include "serializer_prototype.h"


class ImmutableIntervalSet {

    friend class DataSizeTester<ImmutableIntervalSet>;
    friend class BinarySerializer<ImmutableIntervalSet>;

  public:

    class ImmutableIntervalSetBuilder;

  private:

    int size;
    int* endings;

  private:

    ImmutableIntervalSet(std::vector<int>& _endings);

    ImmutableIntervalSet(int _size, int* _endings);

    int testDataSize() const;

  public:

    ImmutableIntervalSet(ImmutableIntervalSetBuilder& builder);

    ImmutableIntervalSet(const ImmutableIntervalSet& other);

    ImmutableIntervalSet& operator= (const ImmutableIntervalSet& other);

    ~ImmutableIntervalSet();

    bool contains(int value) const;

};


class ImmutableIntervalSet::ImmutableIntervalSetBuilder {

  private:

    class Ending;

  private:

    std::vector<Ending> endings;

  private:

    std::vector<int> prebuild();

  public:

    void add(int begin, int end);

    ImmutableIntervalSet build();

    void build(ImmutableIntervalSet* set);

};


class ImmutableIntervalSet::ImmutableIntervalSetBuilder::Ending {

  public:

    int number;
    bool isBegin;

  public:

    Ending(int _number, bool _isBegin);

    bool operator < (const Ending& other) const;

};

#endif //COMPRESS_IMMUTABLE_INTERVAL_SET_H
