#ifndef COMPRESS_TEST_IMMUTABLE_CLUSTER_SET_H
#define COMPRESS_TEST_IMMUTABLE_CLUSTER_SET_H

#include <string>


namespace TestImmutableClusterSet {

    bool simpleTest(const std::string& msg);

    bool stressTestOnSingleElement(const std::string& msg);

    bool stressTestOnEmptySet(const std::string& msg);

    bool stressTestGeneralCase(const std::string& msg);

    bool runTests(const std::string& msg);

} // namespace TestImmutableClusterSet

#endif //COMPRESS_TEST_IMMUTABLE_CLUSTER_SET_H
