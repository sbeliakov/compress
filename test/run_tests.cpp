#include "run_tests.h"

#include <iostream>

#include "test_immutable_set.h"
#include "test_immutable_interval_set.h"
#include "test_immutable_cluster_set.h"
#include "test_immutable_creative_bucket.h"
#include "test_creative_storage.h"
#include "test.h"


bool runTests(const std::string& msg) {
    return TestImmutableSet::runTests(msg) &&
            TestImmutableIntervalSet::runTests(msg) &&
            TestImmutableClusterSet::runTests(msg) &&
            TestImmutableCreativeBucket::runTests(msg) &&
            TestCreativeStorage::runTests(msg);
}

void test() {
    runTests("Running tests.\n");
    std::cerr << "Tests run successfully" << std::endl;
}
