#include "test_creative_storage.h"

#include <fstream>
#include <sstream>

#include "../creative_storage.h"
#include "../serializer.h"

bool TestCreativeStorage::realDataTest(const std::string &msg) {
    std::string failMsg = msg + "Real data test failed";
    ImmutableCreativeBucket::ImmutableCreativeBucketBuilder builder;
    double startTime = clock();

    std::string dataSource = "resources/creatives.bin";
    std::string testSource = "resources/test-creatives2";
    
    std::ifstream input(dataSource, std::ios::binary);
    auto set = BinarySerializer<CreativeStorage>::deserialize(input);

    std::ifstream testInput(testSource);

    int testSize;
    testInput >> testSize;

    for (int i = 0; i < testSize; ++i) {
        int dsp, site, result;
        std::string creative;
        testInput >> dsp >> site >> creative >> result;
        std::stringstream error;
        error << failMsg << ": wrong result for ";
        error << "(" << dsp << ", " << site << "), " << creative << ".\n";
        bool got = set.isApproved(dsp, site, creative);
        error << bool(result) << " expected, but " << got << " found.\n";
        check(got == bool(result), error.str());
    }

    int timeAllowed = testSize / 500 / 1000; // 1 sec - 500k queries
    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < timeAllowed, failMsg + ": too slow.");

    return true;
}

bool TestCreativeStorage::runTests(const std::string& msg) {
    std::string failMsg = msg + "Testing CreativeStorage.\n";
    return realDataTest(failMsg);
}