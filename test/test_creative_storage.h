#ifndef COMPRESS_TEST_CREATIVE_STORAGE_H
#define COMPRESS_TEST_CREATIVE_STORAGE_H

#include <string>


namespace TestCreativeStorage {

    bool realDataTest(const std::string& msg);

    bool runTests(const std::string& msg);

} // namespace TestCreativeStorage

#endif //COMPRESS_TEST_CREATIVE_STORAGE_H
