#include "test_immutable_cluster_set.h"

#include "test.h"
#include "../immutable_cluster_set.h"
#include "../constants.h"


bool TestImmutableClusterSet::simpleTest(const std::string& msg) {
    std::string failMsg = msg + "Simple test failed";
    ImmutableClusterSet::ImmutableClusterSetBuilder builder;
    double startTime = clock();

    std::vector<int> diffs;
    diffs.push_back(1); // + 1
    diffs.push_back(2); // + 3
    diffs.push_back(3); // + 6
    diffs.push_back(2); // + 8
    diffs.push_back(1); // + 9

    builder.addClusterByDiffs(0, diffs);
    builder.addClusterByDiffs(10, diffs);
    builder.addClusterByDiffs(20, diffs);
    builder.addClusterByDiffs(30, diffs);
    builder.addClusterByDiffs(40, diffs);

    auto set = builder.build();

    std::vector<int> in;
    in.push_back(0); in.push_back(10); in.push_back(20); in.push_back(30); in.push_back(40);
    in.push_back(1); in.push_back(11); in.push_back(21); in.push_back(31); in.push_back(41);
    in.push_back(3); in.push_back(13); in.push_back(23); in.push_back(33); in.push_back(43);
    in.push_back(6); in.push_back(16); in.push_back(26); in.push_back(36); in.push_back(46);
    in.push_back(8); in.push_back(18); in.push_back(28); in.push_back(38); in.push_back(48);
    in.push_back(9); in.push_back(19); in.push_back(29); in.push_back(39); in.push_back(49);

    std::vector<int> ni;
    ni.push_back(2); ni.push_back(12); ni.push_back(22); ni.push_back(32); ni.push_back(42);
    ni.push_back(4); ni.push_back(14); ni.push_back(24); ni.push_back(34); ni.push_back(44);
    ni.push_back(5); ni.push_back(15); ni.push_back(25); ni.push_back(35); ni.push_back(45);
    ni.push_back(7); ni.push_back(17); ni.push_back(27); ni.push_back(37); ni.push_back(47);

    for (auto i: in) {
        check(set.contains(i), failMsg + ": false negative.");
    }

    for (auto i: ni) {
        check(!set.contains(i), failMsg + ": false positive.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = (9 + 2 * INT_BITS) * 5;
    check(DataSizeTester<ImmutableClusterSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableClusterSet::stressTestOnSingleElement(const std::string& msg) {
    std::string failMsg = msg + "Stress test on single element failed";
    ImmutableClusterSet::ImmutableClusterSetBuilder builder;
    double startTime = clock();
    const int ELEMENT = 50 * 1000;
    const int RANGE = 100 * 1000;

    builder.addClusterByDiffs(ELEMENT, std::vector<int>());

    auto set = builder.build();

    for (int i = 0; i < RANGE; ++i) {
        if (i == ELEMENT) check(set.contains(i), failMsg + ": false negative.");
        else check(!set.contains(i), failMsg + ": false positive.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = 2 * INT_BITS;
    check(DataSizeTester<ImmutableClusterSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableClusterSet::stressTestOnEmptySet(const std::string& msg) {
    std::string failMsg = msg + "Stress test on empty set failed";
    ImmutableClusterSet::ImmutableClusterSetBuilder builder;
    double startTime = clock();
    const int RANGE = 100 * 1000;

    auto set = builder.build();

    for (int i = 0; i < RANGE; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = 0;
    check(DataSizeTester<ImmutableClusterSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableClusterSet::stressTestGeneralCase(const std::string& msg) {
    std::string failMsg = msg + "Stress test on general case failed";
    ImmutableClusterSet::ImmutableClusterSetBuilder builder;
    double startTime = clock();

    std::vector<int> diffs;
    for (int i = 1; i < 100; ++i) diffs.push_back(i);

    std::vector<bool> mask(2 * 100 * 100 * 100, false);

    for (int i = 0; i < 100; ++i) {
        int element = 100 * 100 * i;
        int begin = element;
        mask[begin] = true;
        for (auto diff: diffs) {
            element += diff;
            mask[element] = true;
        }
        builder.addClusterByDiffs(begin, diffs);
    }

    auto set = builder.build();

    for (int i = 0; i < mask.size(); ++i) {
        check(set.contains(i) == mask[i], failMsg + ": wrong result.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = (4950 + 2 * INT_BITS) * 100;
    check(DataSizeTester<ImmutableClusterSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableClusterSet::runTests(const std::string& msg) {
    std::string failMsg = msg + "Testing ImmutableClusterSet.\n";
    return simpleTest(failMsg) &&
            stressTestOnSingleElement(failMsg) &&
            stressTestOnEmptySet(failMsg) &&
            stressTestGeneralCase(failMsg);
}
