#include "test_immutable_interval_set.h"

#include <string>
#include <vector>
#include <iostream>

#include "test.h"
#include "../immutable_interval_set.h"
#include "../constants.h"


bool TestImmutableIntervalSet::simpleTest(const std::string& msg) {
    std::string failMsg = msg + "Simple test failed";
    ImmutableIntervalSet::ImmutableIntervalSetBuilder builder;
    builder.add(1, 101);
    builder.add(2, 6);
    builder.add(200, 301);
    builder.add(250, 351);
    builder.add(400, 401);
    builder.add(500, 502);
    builder.add(501, 503);
    builder.add(600, 601);
    builder.add(601, 605);
    builder.add(700, 701);
    builder.add(700, 701);
    builder.add(800, 800);
    builder.add(900, 905);
    builder.add(901, 901);
    builder.add(900, 901);
    builder.add(901, 905);

    auto set = builder.build();

    std::vector<int> in;
    in.push_back(1);
    in.push_back(2);
    in.push_back(3);
    in.push_back(5);
    in.push_back(6);
    in.push_back(100);
    in.push_back(200);
    in.push_back(201);
    in.push_back(249);
    in.push_back(250);
    in.push_back(251);
    in.push_back(299);
    in.push_back(300);
    in.push_back(301);
    in.push_back(349);
    in.push_back(350);
    in.push_back(400);
    in.push_back(500);
    in.push_back(501);
    in.push_back(502);
    in.push_back(600);
    in.push_back(601);
    in.push_back(603);
    in.push_back(604);
    in.push_back(700);
    in.push_back(900);
    in.push_back(901);
    in.push_back(902);
    in.push_back(903);
    in.push_back(904);

    std::vector<int> ni;
    ni.push_back(0);
    ni.push_back(101);
    ni.push_back(199);
    ni.push_back(351);
    ni.push_back(399);
    ni.push_back(401);
    ni.push_back(499);
    ni.push_back(503);
    ni.push_back(599);
    ni.push_back(605);
    ni.push_back(699);
    ni.push_back(701);
    ni.push_back(799);
    ni.push_back(800);
    ni.push_back(801);
    ni.push_back(899);
    ni.push_back(905);

    for (auto i: in) {
        check(set.contains(i), failMsg + ": false negative.");
    }
    for (auto i: ni) {
        check(!set.contains(i), failMsg + ": false positive.");
    }

    int controlSize = 2 * INT_BITS * 7;
    check(DataSizeTester<ImmutableIntervalSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableIntervalSet::stressTestEmptySet(const std::string& msg) {
    std::string failMsg = msg + "Stress test on empty interval set failed";
    auto set = ImmutableIntervalSet::ImmutableIntervalSetBuilder().build();

    double startTime = clock();

    for (int i = 0; i < 100 * 1000; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = 0;
    check(DataSizeTester<ImmutableIntervalSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableIntervalSet::testOverlapping(const std::string& msg) {
    std::string failMsg = msg + "Overlapping test failed";
    ImmutableIntervalSet::ImmutableIntervalSetBuilder builder;
    double startTime = clock();

    const int NUM_INTERVALS = 100 * 1000;
    const int INTERVAL_LEN = 100;
    const int INTERVAL_SHIFT = 1;
    const int SET_BEGIN = 50 * 1000;
    const int SET_END = SET_BEGIN + INTERVAL_LEN + (NUM_INTERVALS - 1) * INTERVAL_SHIFT;

    for (int i = 0; i < NUM_INTERVALS; ++i) {
        builder.add(SET_BEGIN + i * INTERVAL_SHIFT, SET_BEGIN + i * INTERVAL_SHIFT + INTERVAL_LEN);
    }
    auto set = builder.build();

    for (int i = 0; i < SET_BEGIN; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }
    for (int i = SET_BEGIN; i < SET_END; ++i) {
        check(set.contains(i), failMsg + ": false negative.");
    }
    for (int i = SET_END; i < 2 * SET_END; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = 2 * INT_BITS;
    check(DataSizeTester<ImmutableIntervalSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableIntervalSet::testEmptyIntervals(const std::string& msg) {
    std::string failMsg = msg + "Test on empty intervals failed";
    ImmutableIntervalSet::ImmutableIntervalSetBuilder builder;
    double startTime = clock();

    const int NUM_INTERVALS = 100 * 1000;
    const int INTERVAL_LEN = 0;
    const int INTERVAL_SHIFT = 10;
    const int SET_BEGIN = 50 * 1000;
    const int SET_END = SET_BEGIN + INTERVAL_LEN + (NUM_INTERVALS - 1) * INTERVAL_SHIFT;

    for (int i = 0; i < NUM_INTERVALS; ++i) {
        builder.add(SET_BEGIN + i * INTERVAL_SHIFT, SET_BEGIN + i * INTERVAL_SHIFT + INTERVAL_LEN);
    }
    auto set = builder.build();

    for (int i = 0; i < 2 * SET_END; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = 0;
    check(DataSizeTester<ImmutableIntervalSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableIntervalSet::generalTest(const std::string& msg) {
    std::string failMsg = msg + "General test failed";
    ImmutableIntervalSet::ImmutableIntervalSetBuilder builder;
    double startTime = clock();

    const int NUM_INTERVALS = 1000;
    const int INTERVAL_LEN = 100;
    const int INTERVAL_SHIFT = 200;
    const int SET_BEGIN = 50 * 1000;
    const int SET_END = SET_BEGIN + INTERVAL_LEN + (NUM_INTERVALS - 1) * INTERVAL_SHIFT;

    for (int i = 0; i < NUM_INTERVALS; ++i) {
        builder.add(SET_BEGIN + i * INTERVAL_SHIFT, SET_BEGIN + i * INTERVAL_SHIFT + INTERVAL_LEN);
    }
    auto set = builder.build();

    std::vector<bool> mask(2 * SET_END, false);

    for (int i = 0; i < NUM_INTERVALS; ++i) {
        for (int j = SET_BEGIN + i * INTERVAL_SHIFT; j < SET_BEGIN + i * INTERVAL_SHIFT + INTERVAL_LEN; ++j) {
            mask[j] = true;
        }
    }

    for (int i = 0; i < mask.size(); ++i) {
        check(set.contains(i) == mask[i], failMsg + ": wrong value.");
    }

    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = 2 * INT_BITS * NUM_INTERVALS;
    check(DataSizeTester<ImmutableIntervalSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableIntervalSet::runTests(const std::string& msg) {
    std::string failMsg = msg + "Testing ImmutableIntervalSet.\n";
    return simpleTest(failMsg) &&
            stressTestEmptySet(failMsg) &&
            testOverlapping(failMsg) &&
            testEmptyIntervals(failMsg) &&
            generalTest(failMsg);
}
