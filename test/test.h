#ifndef COMPRESS_TEST_H
#define COMPRESS_TEST_H

#include <string>


bool check(bool condition, const std::string& message);

template <class SomeSet>
class DataSizeTester {
public:
    static int testDataSize(const SomeSet& entity) {
        return entity.testDataSize();
    }
};

#endif //COMPRESS_TEST_H
