#ifndef COMPRESS_TEST_IMMUTABLE_INTERVAL_SET_H
#define COMPRESS_TEST_IMMUTABLE_INTERVAL_SET_H

#include <string>


namespace TestImmutableIntervalSet {

    bool simpleTest(const std::string& msg);

    bool runTests(const std::string& msg);

    bool stressTestEmptySet(const std::string& msg);

    bool testOverlapping(const std::string& msg);

    bool testEmptyIntervals(const std::string& msg);

    bool generalTest(const std::string& msg);

} // namespace TestImmutableIntervalSet

#endif //COMPRESS_TEST_IMMUTABLE_INTERVAL_SET_H
