#include "test.h"

#include <iostream>
#include <cassert>
#include <string>


bool check(bool condition, const std::string& message) {
    if (!condition) {
        std::cerr << message << std::endl;
    }
    assert(condition);
    return condition;
}
