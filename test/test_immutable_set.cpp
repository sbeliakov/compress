#include "test_immutable_set.h"

#include <iostream>
#include <ctime>

#include "test.h"
#include "../immutable_set.h"
#include "../constants.h"


bool TestImmutableSet::simpleTest(const std::string& msg) {
    std::string failMsg = msg + "Simple test failed";
    auto builder = ImmutableSet::ImmutableSetBuilder();
    std::vector<int> inside;
    inside.push_back(1);
    inside.push_back(2);
    inside.push_back(3);
    inside.push_back(4);
    inside.push_back(5);
    inside.push_back(6);
    inside.push_back(6);
    inside.push_back(5);
    inside.push_back(4);
    inside.push_back(3);

    std::vector<int> outside;
    outside.push_back(7);
    outside.push_back(8);
    outside.push_back(9);

    for (auto i: inside) {
        builder.add(i);
    }

    auto set = builder.build();

    for (auto i: inside) {
        check(set.contains(i), failMsg + ": false negative.");
    }
    for (auto i: outside) {
        check(!set.contains(i), failMsg + ": false positive.");
    }

    int controlSize = INT_BITS * 6;
    check(DataSizeTester<ImmutableSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableSet::stressTest(const std::string& msg) {
    std::string failMsg = msg + "Stress test with 1..100'000 failed";
    double startTime = clock();
    auto builder = ImmutableSet::ImmutableSetBuilder();
    const int LIMIT = 100 * 1000;
    for (int i = 0; i < LIMIT; ++i) {
        builder.add(i);
    }
    auto set = builder.build();
    for (int i = 0; i < LIMIT; ++i) {
        check(set.contains(i), failMsg + ": false negative.");
    }
    for (int i = LIMIT; i < 2 * LIMIT; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }
    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = INT_BITS * LIMIT;
    check(DataSizeTester<ImmutableSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableSet::stressTestWithSingleElement(const std::string& msg) {
    std::string failMsg = msg + "Stress test with single element failed";
    double startTime = clock();
    auto builder = ImmutableSet::ImmutableSetBuilder();
    const int TIMES = 100 * 1000;
    const int ELEMENT = 50 * 1000;
    for (int i = 0; i < TIMES; ++i) {
        builder.add(ELEMENT);
    }

    auto set = builder.build();

    for (int i = 0; i < ELEMENT; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }
    check(set.contains(ELEMENT), failMsg + ": false negative.");
    for (int i = ELEMENT + 1; i < TIMES; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }
    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = INT_BITS;
    check(DataSizeTester<ImmutableSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableSet::testEmptySet(const std::string& msg) {
    std::string failMsg = msg + "Test of empty set failed";
    double startTime = clock();
    auto set = ImmutableSet::ImmutableSetBuilder().build();
    for (int i = 0; i < 100 * 1000; ++i) {
        check(!set.contains(i), failMsg + ": false positive.");
    }
    double timePassed = (clock() - startTime) / CLOCKS_PER_SEC;
    check(timePassed < 1., failMsg + ": too slow.");

    int controlSize = 0;
    check(DataSizeTester<ImmutableSet>::testDataSize(set) == controlSize, failMsg + ": wrong data size.");

    return true;
}

bool TestImmutableSet::runTests(const std::string& msg) {
    std::string failMsg = msg + "Testing ImmutableSet.\n";
    return simpleTest(failMsg) &&
           stressTest(failMsg) &&
           stressTestWithSingleElement(failMsg) &&
           testEmptySet(failMsg);
}
