#ifndef COMPRESS_TEST_IMMUTABLE_SET_H
#define COMPRESS_TEST_IMMUTABLE_SET_H

#include <string>


namespace TestImmutableSet {

    bool simpleTest(const std::string& msg);

    bool stressTest(const std::string& msg);

    bool stressTestWithSingleElement(const std::string& msg);

    bool testEmptySet(const std::string& msg);

    bool runTests(const std::string& msg);

} // namespace TestImmutableSet

#endif //COMPRESS_TEST_IMMUTABLE_SET_H
