#ifndef COMPRESS_TEST_IMMUTABLE_CREATIVE_BUCKET_H
#define COMPRESS_TEST_IMMUTABLE_CREATIVE_BUCKET_H

#include "test.h"
#include "../immutable_creative_bucket.h"


namespace TestImmutableCreativeBucket {

    bool simpleTest(const std::string& msg);

    bool intervalTest(const std::string &msg);

    bool clusterTest(const std::string &msg);

    bool singleElementTest(const std::string &msg);

    bool emptyBucketTest(const std::string &msg);

    bool realDataTest(const std::string &msg);

    bool runTests(const std::string& msg);

} // namespace TestImmutableCreativeBucket

#endif //COMPRESS_TEST_IMMUTABLE_CREATIVE_BUCKET_H
