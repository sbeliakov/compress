#include "serializer.h"

#include "constants.h"


void BinarySerializer<std::string>::serialize(const std::string& value, std::ostream& out) {
    BinarySerializer<size_t>::serialize(value.size(), out);
    for (char c: value) {
        BinarySerializer<char>::serialize(c, out);
    }
}

std::string BinarySerializer<std::string>::deserialize(std::istream& in) {
    size_t size = BinarySerializer<size_t>::deserialize(in);
    std::string result(size, 'a');
    for (int i = 0; i < size; ++i) {
        result[i] = BinarySerializer<char>::deserialize(in);
    }
    return result;
}


void BinarySerializer<CreativeStorage>::serialize(const CreativeStorage& value, std::ostream& out) {
    BinarySerializer<std::unordered_map<CreativeBucketKey, CreativeBucketWithMeta>>::serialize(value.buckets, out);
    BinarySerializer<std::unordered_map<std::string, int>>::serialize(value.strCreativesToInt, out);
}

CreativeStorage BinarySerializer<CreativeStorage>::deserialize(std::istream& in) {
    auto buckets = BinarySerializer<std::unordered_map<CreativeBucketKey, CreativeBucketWithMeta>>::deserialize(in);
    auto strCreativesToInt = BinarySerializer<std::unordered_map<std::string, int>>::deserialize(in);
    return CreativeStorage(buckets, strCreativesToInt);
}


void BinarySerializer<CreativeBucketWithMeta>::serialize(const CreativeBucketWithMeta &value, std::ostream &out) {
    BinarySerializer<ImmutableCreativeBucket>::serialize(value.bucket, out);
    BinarySerializer<int>::serialize(value.meta, out);
}

CreativeBucketWithMeta BinarySerializer<CreativeBucketWithMeta>::deserialize(std::istream &in) {
    auto bucket = BinarySerializer<ImmutableCreativeBucket>::deserialize(in);
    auto meta = BinarySerializer<int>::deserialize(in);
    return CreativeBucketWithMeta(bucket, meta);
}


void BinarySerializer<ImmutableCreativeBucket>::serialize(const ImmutableCreativeBucket &value, std::ostream &out) {
    BinarySerializer<ImmutableClusterSet>::serialize(value.clusters, out);
    BinarySerializer<ImmutableIntervalSet>::serialize(value.intervals, out);
    BinarySerializer<ImmutableSet>::serialize(value.singles, out);
}

ImmutableCreativeBucket BinarySerializer<ImmutableCreativeBucket>::deserialize(std::istream &in) {
    auto clusters = BinarySerializer<ImmutableClusterSet>::deserialize(in);
    auto intervals = BinarySerializer<ImmutableIntervalSet>::deserialize(in);
    auto singles = BinarySerializer<ImmutableSet>::deserialize(in);
    return ImmutableCreativeBucket(clusters, intervals, singles);
}


void BinarySerializer<ImmutableClusterSet>::serialize(const ImmutableClusterSet &value, std::ostream &out) {
    BinarySerializer<int>::serialize(value.numClusters, out);
    BinarySerializer<int>::serialize(value.maskSize, out);
    for (int i = 0; i < value.numClusters; ++i) {
        BinarySerializer<int>::serialize(value.clusterBegin[i], out);
    }
    for (int i = 0; i < value.numClusters; ++i) {
        BinarySerializer<int>::serialize(value.clusterBeginPos[i], out);
    }
    int maskInts = (value.maskSize + INT_BITS - 1) / (INT_BITS);
    for (int i = 0; i < maskInts; ++i) {
        BinarySerializer<int>::serialize(value.mask[i], out);
    }
}

ImmutableClusterSet BinarySerializer<ImmutableClusterSet>::deserialize(std::istream &in) {
    int numClusters = BinarySerializer<int>::deserialize(in);
    int maskSize = BinarySerializer<int>::deserialize(in);
    int* clusterBegin = new int[numClusters];
    for (int i = 0; i < numClusters; ++i) {
        clusterBegin[i] = BinarySerializer<int>::deserialize(in);
    }
    int* clusterBeginPos = new int[numClusters];
    for (int i = 0; i < numClusters; ++i) {
        clusterBeginPos[i] = BinarySerializer<int>::deserialize(in);
    }
    int maskInts = (maskSize + INT_BITS - 1) / (INT_BITS);
    int* mask = new int[maskInts];
    for (int i = 0; i < maskInts; ++i) {
        mask[i] = BinarySerializer<int>::deserialize(in);
    }
    return ImmutableClusterSet(clusterBegin, clusterBeginPos, mask, numClusters, maskSize);
}


void BinarySerializer<ImmutableIntervalSet>::serialize(const ImmutableIntervalSet &value, std::ostream &out) {
    BinarySerializer<int>::serialize(value.size, out);
    for (int i = 0; i < value.size; ++i) {
        BinarySerializer<int>::serialize(value.endings[i], out);
    }
}

ImmutableIntervalSet BinarySerializer<ImmutableIntervalSet>::deserialize(std::istream &in) {
    int size = BinarySerializer<int>::deserialize(in);
    int* endings = new int[size];
    for (int i = 0; i < size; ++i) {
        endings[i] = BinarySerializer<int>::deserialize(in);
    }
    return ImmutableIntervalSet(size, endings);
}


void BinarySerializer<ImmutableSet>::serialize(const ImmutableSet &value, std::ostream &out) {
    BinarySerializer<int>::serialize(value.size, out);
    for (int i = 0; i < value.size; ++i) {
        BinarySerializer<int>::serialize(value.numbers[i], out);
    }
}

ImmutableSet BinarySerializer<ImmutableSet>::deserialize(std::istream &in) {
    int size = BinarySerializer<int>::deserialize(in);
    int* numbers = new int[size];
    for (int i = 0; i < size; ++i) {
        numbers[i] = BinarySerializer<int>::deserialize(in);
    }
    return ImmutableSet(numbers, size);
}
